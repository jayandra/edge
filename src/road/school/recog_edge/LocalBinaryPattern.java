package road.school.recog_edge;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import android.util.Log;

public class LocalBinaryPattern {
	private int radius = 1;
	private int neighbours = 8;
	
	public LocalBinaryPattern(){
		
	}
	public LocalBinaryPattern(int r, int n){
		radius = r;
		neighbours = n;
	}
	
	/**
	 * Compute LBP using the radius 1 and 8 neighbors. 
	 * By default it uses the binary value, but quantization to bins of 15 and grey code can also be used.
	 * 
	 * @param source_image, use_bins_principle, use_grey_coding
	 * @return
	 */
	public Mat computeLBP(Mat source_image, boolean bins, boolean grey){
		Mat bw = source_image.clone();
		Imgproc.cvtColor(source_image, bw, Imgproc.COLOR_RGB2GRAY);
		Mat lbp_output = bw.clone();
//		Log.e("1-1", Integer.toString( bw.get(1, 1).length) );
//		Log.e("1-2  col rows", Integer.toString( bw.cols())+"  "+Integer.toString(bw.rows()) );
		int temp_center=0;
		int center;
//		int i;
//		i = 1<<7;
//		Log.e("<<7", Integer.toBinaryString((int) i) );
//		i |= 1<<5;
//		Log.e("<<5", Integer.toBinaryString((int) i) );
//		i |= 1<<4;
//		Log.e("<<4", Integer.toBinaryString((int) i) );
//		Log.e("<<4", Integer.toString((int) i) );
		
		for(int x = 200; x < 440; x++){				//bw.cols()-1 : 640
			for(int y = 300; y < bw.rows()-1 ; y++){			//bw.rows()-1 : 480
				
				temp_center = 0;
				center = (int) bw.get(y,x)[0];
				
				temp_center |= (bw.get(y-1,x-1)[0] > center) ? (1<<7) : (0<<7);
				temp_center |= (bw.get(y-1,x)[0] > center) ? (1<<6) : (0<<6);
				temp_center |= (bw.get(y-1,x+1)[0] > center) ? (1<<5) : (0<<5);
				temp_center |= (bw.get(y,x+1)[0] > center) ? (1<<4) : (0<<4);
				temp_center |= (bw.get(y+1,x+1)[0] > center) ? (1<<3) : (0<<3);
				temp_center |= (bw.get(y+1,x)[0] > center) ? (1<<2) : (0<<2);
				temp_center |= (bw.get(y+1,x-1)[0] > center) ? (1<<1) : (0<<1);
				temp_center |= (bw.get(y,x-1)[0] > center) ? (1<<0) : (0<<0);
				
				if(grey == true){
				//conversion of binary to grey code
					temp_center = (temp_center >>1 ) ^ temp_center;
//					Log.e("grey true", Integer.toString(temp_center));
				}
				if(bins == true){
					temp_center = (temp_center / 15) * 15;
//					Log.e("bins true", Integer.toString(temp_center));
				}
				
				lbp_output.put(y, x, temp_center);
			}
		}
		return lbp_output;
	}
	
	/*
	 * Computes the histogram for a bin size of 32.
	 * The histogram are stored in format [ [0th bin],[1st bin],[],....... [31st bin]]
	 * 
	 * @param grayscale_image
	 * @return Mat_of_bin_values
	 */
	public Mat compute_histogram(Mat grayscale_image){
		List<Mat> imagesList=new ArrayList<Mat>();
        imagesList.add(grayscale_image);
        int channelArray[]={0};
        MatOfInt channels_list = new MatOfInt(0);
        Mat hist=new Mat();
        
        Imgproc.calcHist(imagesList, channels_list, new Mat(), hist, new MatOfInt(32), new MatOfFloat(0,255));

        for (int i = 0; i< 32; i++) {
            double[] histValues = hist.get(i, 0);
            for (int j = 0; j < histValues.length; j++) {
                Log.e("Histogram", i+"  "+j+"  yourData=" + histValues[j]);
            }
        }
        return hist;	
	}
	
	public Mat computeNLBP(Mat source_image){
		Mat bw = source_image.clone();
		
//		a = "110110010";
//		changes = 0
//		for(i=0; i<a.length-1; i++){
//		    console.log(a[i]);
//		    if( a[i] != a[i+1]){
//		        changes++;
//		        console.log(i+"   a[i] "+a[i]+"   a[i+1] "+a[i+1]);
//		    }
//		}
//		if( a[a.length-1] != a[0]){
//		        changes++;
//		        console.log(a.length+" --  a[i] "+a[a.length-1]+"   a[i+1] "+a[0]);
//		    }
		
		return bw;
	}
}
