package road.school.recog_edge;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.util.Log;

/**
 * Edge detection operations
 * Parameters: UpperThreshold and LowerThreshold for canny edge *
 */
public class EdgeDetection {
	public int UpperThreshold;
	public int LowerThreshold;
	private int threshold_ratio = 2;
	private int grid_columns_number = 20;
	private int grid_rows_number = 10;
	private double largest_rectangle_area = 303848.5; //determined by using a log
	private Point test_point = new Point(330,425);
	
	public EdgeDetection(){
		LowerThreshold = 125;
		UpperThreshold = LowerThreshold * threshold_ratio;
	}
	
	/**
	 * Computes the contours of an image and returns the list of contours found
	 * 
	 * @param source_image
	 * @return contours_list
	 */
	public List<MatOfPoint> compute_contours(Mat source_image){
		Mat bw = source_image.clone();
		int bw_maxX= bw.width();
		int bw_maxY= bw.height();
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat mHierarchy = new Mat();
Log.e("Value of threshold",Integer.toString(LowerThreshold)+"         "+Integer.toString(LowerThreshold * threshold_ratio));
		Imgproc.cvtColor(source_image, bw, Imgproc.COLOR_RGB2GRAY);
		Imgproc.blur(bw, bw, new Size(4,4));
//		Imgproc.Canny(bw, bw, LowerThreshold, LowerThreshold * threshold_ratio);
		Imgproc.Canny(bw, bw, LowerThreshold, LowerThreshold * threshold_ratio, 3, true);
		Core.line(bw, new Point(2,2), new Point(bw_maxX-2,2), new Scalar(255, 255, 255), 2 );	//top
		Core.line(bw, new Point(2,bw_maxY-2), new Point(bw_maxX-2,bw_maxY-2), new Scalar(255, 255, 255), 2 ); //bottom
		Core.line(bw, new Point(2,2), new Point(2,bw_maxY-2), new Scalar(255, 255, 255), 2 ); //left
		Core.line(bw, new Point(bw_maxX-2,2), new Point(bw_maxX-2,bw_maxY-2), new Scalar(255, 255, 255), 2 ); //right
		Imgproc.findContours(bw, contours, mHierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
		
		return contours;
	}

	/**
	 * Determines the road contour by assuming the reference point to be in the road region
	 * 
	 * @param contours
	 * @return index_of_road_contour
	 */
	public int find_road(List<MatOfPoint> contours){
		MatOfPoint2f road_contour;
		double is_road;			// gives 0 if the point is inside contour else 0
		int road_idx = -1;
		
		for (int idx = 0; idx < contours.size(); idx++) {
			road_contour = new MatOfPoint2f( contours.get(idx).toArray() );
			is_road = Imgproc.pointPolygonTest(road_contour, test_point, false);
		    if(is_road == 1 && Imgproc.contourArea(contours.get(idx)) < largest_rectangle_area){
		    	road_idx = idx;
		    	break;
		    }
		}
		return road_idx;
	}
	
	/**
	 * Draws the given contour from the list of contours provided
	 * @param source_image
	 * @param contours
	 * @param road_contour_idx
	 */
	public void draw_contour(Mat source_image, List<MatOfPoint> contours, int road_contour_idx){
		Imgproc.drawContours(source_image, contours, road_contour_idx, new Scalar(255, 255, 255),2);
	}
	
	/**
	 * Draws an overlay grid of grid_rows_number*grid_columns_number over the image and a circle around point assumed to be road
	 * 
	 * @param source_image
	 * @param contours
	 * @param road_contour_idx
	 */
	public void draw_overlay(Mat source_image, List<MatOfPoint> contours, int road_contour_idx){
		int grid_width = source_image.cols()/grid_columns_number;
		int grid_height = source_image.rows()/grid_rows_number;
		MatOfPoint2f road_contour = new MatOfPoint2f( contours.get(road_contour_idx).toArray() );
		double u1, u2, c, l1, l2, prob;
		
		Core.circle(source_image, test_point, 5, new Scalar(220,220,10), -1);
		for(int x = 0; x < source_image.cols(); x+=grid_width){	
			Core.line(source_image, new Point(x,0), new Point(x,source_image.rows()), new Scalar(255, 255, 255),1);	//drawing of vertical lines
			for(int y = 0; y < source_image.rows() ; y+=grid_height){
				Core.line(source_image, new Point(0,y), new Point(source_image.cols(),y), new Scalar(255, 255, 255),1);
				
				u1 = Imgproc.pointPolygonTest(road_contour, new Point(x,y), false);
				u2 = Imgproc.pointPolygonTest(road_contour, new Point(x+grid_width, y), false);
				l1 = Imgproc.pointPolygonTest(road_contour, new Point(x,y+grid_height), false);
				l2 = Imgproc.pointPolygonTest(road_contour, new Point(x+grid_width, y+grid_height), false);
				c = Imgproc.pointPolygonTest(road_contour, new Point(x+(grid_width/2), y+(grid_height/2)), false);
				prob = u1+u2+c+l1+l2;
				// It is Safe
				if(prob > 0){
					Core.putText(source_image, "~", new Point(x+(grid_width/2), y+(grid_height/2)), 3, 1, new Scalar(255, 255, 255),1);
					//Core.rectangle(source_image, new Point(x,y), new Point(x+grid_width, y+grid_height), new Scalar(46, 255, 4), -1);
				}
				if(prob == 0){
					Core.putText(source_image, ".", new Point(x+(grid_width/2), y+(grid_height/2)), 3, 1, new Scalar(255, 255, 255),1);
					//Core.rectangle(source_image, new Point(x,y), new Point(x+grid_width, y+grid_height), new Scalar(220,220,10), -1);
				}
				// It is Un-Safe
				if(prob < 0){
					//Core.putText(source_image, "-", new Point(x+(grid_width/2), y+(grid_height/2)), 3, 1, new Scalar(255, 255, 255),1);
					//Core.rectangle(source_image, new Point(x,y), new Point(x+grid_width, y+grid_height), new Scalar(46, 255, 4), -1);
				}
				//Core.putText(bw, String.valueOf(counter), new Point(x+(grid_width/2), y+(grid_height/2)), 3, 1, new Scalar(255, 255, 255),1);
				//Core.addWeighted(overlay, 0.4,source_image, 1, 0, source_image);
			}
		}
	}

	/**
	 * Computes the boundries of contours and labels the corresponding squares with 1
	 * @param source_image
	 * @param contours
	 * 
	 * process: computes the contour of top-left corner and uses that idx to check if all 3-corners fall in the same contour
	 * drawback: If the top-left point falls in larger contour, the remaining 3 are shown to be in the same contour even if some
	 * 			 of them fall in the sub-contour
	 */
	public void find_bounderies(Mat source_image, List<MatOfPoint> contours){
		int grid_width = source_image.cols()/grid_columns_number;
		int grid_height = source_image.rows()/grid_rows_number;
		double u1, u2, l1, l2;
		int old_x = 0, old_y = 0, new_x, new_y;
		int top_left_contour_idx;
		MatOfPoint2f road_contour;
//Log.e("size of contours", Integer.toString(contours.size()));		
		for(int x = 0; x < source_image.cols(); x+=grid_width){
			Core.line(source_image, new Point(x,0), new Point(x,source_image.rows()), new Scalar(255, 255, 255),1);	//drawing of vertical lines
			for(int y = 0; y < source_image.rows() ; y+=grid_height){
				Core.line(source_image, new Point(0,y), new Point(source_image.cols(),y), new Scalar(255, 255, 255),1);
				
				// Find the contour that encloses the top left corner. Use that contour to check if the remaining 3 points.
				top_left_contour_idx = -5;
				u1 = -1;
				
				old_x = x;
				old_y = y;
				for (int idx = 0; idx < contours.size(); idx++) {
					road_contour = new MatOfPoint2f( contours.get(idx).toArray() );
					
					if(old_x == 0){
						old_x = 5;
					}
					if(old_y == 0){
						old_y = 5;
					}
					u1 = Imgproc.pointPolygonTest(road_contour, new Point(old_x, old_y), false);
					
					if(u1 == 1){
						top_left_contour_idx = idx;
						break;
					}
				}

				if(top_left_contour_idx != -5){
					road_contour = new MatOfPoint2f( contours.get(top_left_contour_idx).toArray() );

					new_x = x+grid_width;
					new_y = y+grid_height;
					if(new_x >= 640){
						new_x = 640-5;			// -4 because we have drawn outer bounding rectangle of width 2 pixel
					}
					if(new_y >= 480){
						new_y = 480-5;
					}
					
					u1 = Imgproc.pointPolygonTest(road_contour, new Point(old_x, old_y), false);
					u2 = Imgproc.pointPolygonTest(road_contour, new Point(new_x, old_y), false);
					l1 = Imgproc.pointPolygonTest(road_contour, new Point(old_x,new_y), false);
					l2 = Imgproc.pointPolygonTest(road_contour, new Point(new_x, new_y), false);
//					c = Imgproc.pointPolygonTest(road_contour, new Point(x+(grid_width/2), y+(grid_height/2)), false);
// Log.e("u...l for "+Integer.toString(old_x)+"  "+Integer.toString(new_x)+"  "+Integer.toString(old_y)+"  "+Integer.toString(new_y), 
//		 Double.toString(u1)+" "+Double.toString(u2)+" "+Double.toString(l1)+" "+Double.toString(l2));	
//Log.e("------------","-----------");
//	Log.e("ccv", 
//		Integer.toString(top_left_contour_idx)+"    :    "+
//		Integer.toString(old_x)+","+Integer.toString(old_y)+" =>"+Double.toString(u1)+"  "+
//		Integer.toString(new_x)+","+Integer.toString(old_y)+" =>"+Double.toString(u2)+"  "+
//		Integer.toString(old_x)+","+Integer.toString(new_y)+" =>"+Double.toString(l1)+"  "+
//		Integer.toString(new_x)+","+Integer.toString(new_y)+" =>"+Double.toString(l2)+"  "
//	 );					
					
					
					if(u1!=u2 || u2!=l1 || l1!=l2 || l2 != u1){
						Core.putText(source_image, "1", new Point(old_x+(grid_width/2), old_y+(grid_height/2)), 3, 1, new Scalar(255, 255, 255),1);
					}
				}
			}
		}	
	}

}
