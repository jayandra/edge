package road.school.recog_edge;

import android.util.Log;

public class RoadHistogram {
	public int numBins = 30;
	public int[][] histogram = new int[numBins][numBins];
	public int total = 0;
	public double threshhold = .2;
	public int max = 0;
	
	
	
	public RoadHistogram(){
		
	}
	public void addPixel( double red, double green ){
		int x = (int)((red / 255) * (numBins-1));
		int y = (int)((green / 255) * (numBins-1));
		//Log.e("AP", "Red: " + red + " x: " + x + "\n"+ "Green: " + green + " y: " + y);
		histogram[x][y]++;
		total++;
	}
	
	public double getRoadDegree(double red, double green){
		int x = (int)((red / 255) * (numBins-1));
		int y = (int)((green / 255) * (numBins-1));
		//double retVal = histogram[x][y]/(double)total;
		//Log.e("GRD", " return: " + retVal);
		return histogram[x][y]/(double)max;
	}
	
	public boolean isRoad(double red, double green){
		int x = (int)((red / 255) * (numBins-1));
		int y = (int)((green / 255) * (numBins-1));
		if ( histogram[x][y] / (double)max < threshhold ){
			return true;
		}else{
			return false;
		}
	}
	
	
	public void findMax(){
		max = histogram[0][0];
		for(int i = 0; i < numBins; i++){
			for(int j = 0; j < numBins; j++){
				if(histogram[i][j] > max){
					max = histogram[i][j];
				}
			}
		}
	}
	
	
	public void print(){
		String msg = "";
		msg += total;
		msg += "\n";
		for (int i = 0; i < numBins; i++){
			for(int j = 0; j < numBins; j++){
				msg += histogram[i][j];
				msg += " ";
			}
			msg += "\n";
		}
		
		Log.e("Print", msg);
	}
	
	
	
}
