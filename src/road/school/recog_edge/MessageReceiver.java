package road.school.recog_edge;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class MessageReceiver extends Thread{
	
	private final BluetoothServerSocket mmServerSocket;
    public BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private static final UUID sf_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B35FB");
    public InputStream m_input;
    public String message;
    public boolean connected = false;
    public boolean inCreated = false;
    
    
    public MessageReceiver() {
        // Use a temporary object that is later assigned to mmServerSocket,
        // because mmServerSocket is final
    	
        BluetoothServerSocket tmp = null;
        try {
            // MY_UUID is the app's UUID string, also used by the client code
            tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(mBluetoothAdapter.getName(), sf_UUID);
            //Log.e("in acceptThread", "seversocket made");
        } catch (IOException e) { 
        	//Log.e("in acceptThread", "server socket failed");
        	}
        mmServerSocket = tmp;
    }
 
    public void run() {
        BluetoothSocket socket = null;
        // Keep listening until exception occurs or a socket is returned
        while (!connected && !inCreated) {
        	//Log.e("in acceptThread", "Trying to accept");
        	
            try {
                socket = mmServerSocket.accept();
                //Log.e("in acceptThread", "accepted");
            } catch (IOException e) {
            	//Log.e("in acceptThread", "not accepted");
            }
            // If a connection was accepted
            if (socket != null) {
                // Do work to manage the connection (in a separate thread)
            	connected = true;
                try {
                	//Log.e("in acceptThread", "connection accepted, opening input stream");
					m_input = socket.getInputStream();
					inCreated = true;
				} catch (IOException e) {
					
				}                
            }
        }
    }
    
    public boolean ready(){
    	try {
			if(m_input.available() > 2){
				return true;
			}else{
				return false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return false;
		}
    }
    public char[] read(){
    	char[] msg = new char[4];
    	msg[0] = (char)300;
    	msg[1] = (char)300;
    	msg[2] = (char)300;
    	msg[3] = (char)300;
    	try {
				if(m_input.available() > 2){
					msg[0] = (char)m_input.read();
			    	msg[1] = (char)m_input.read();
			    	msg[2] = (char)m_input.read();
			    	msg[3] = (char)m_input.read();
					
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				
			}
    	Log.e("MR", "message: " + msg[0] + " "+ msg[1] + " "+ msg[2] + " "+ msg[3]);
		return msg;
    	
    }
    
   
    /** Will cancel the listening socket, and cause the thread to finish */
    public void cancel() {
        try {
            mmServerSocket.close();
        } catch (IOException e) { }
    }
}