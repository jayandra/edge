package road.school.recog_edge;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import android.util.Log;

public class RoadModel {
	//private List< List<Double[]>> colorModels = new ArrayList<List<Double[]>>();
	private List< List<Mat>> colorModels = new ArrayList<List<Mat>>();
	
	private final int MaxModels = 5;
	
	private final static int RED = 0;
	private final static int GREEN = 1;
	private final int intCompareMethod = Imgproc.CV_COMP_CHISQR;  //correlation chi-squred  intersection  Bhattacharyya
	
	private final double SIMILARITY_THRESHOLD = .75;
	
	public double calculateRoadProbability(int red, int green){
		double dblRoadProbability = 0.0;
		
		for(int i = 0; i < colorModels.size(); i++){
			int redBin = (int) (red/8) ;
			int greenBin = (int) (green/8) ;
			
			double redProb = colorModels.get(i).get(RED).get(redBin, 0)[0];
			double greenProb = colorModels.get(i).get(GREEN).get(greenBin, 0)[0];
			
			//Log.i("RoadModel.calculateRoadProbability", "Red Prob: " +redProb + "  Green Prob: " + greenProb);
			double totalProb = redProb * greenProb;
			if(totalProb > dblRoadProbability){
				dblRoadProbability = totalProb;
			}
		}
		
		
		return dblRoadProbability;
	}
	public void addModel(Mat newRedHistogram, Mat newGreenHistogram){
			List<Mat> newHistograms = new ArrayList<Mat>();
			newHistograms.add(newRedHistogram);
			newHistograms.add(newGreenHistogram);
			
			if(colorModels.size() == 0){
				colorModels.add(newHistograms);
				
			}
			else if(colorModels.size() <= MaxModels){
				//Log.i("RoadModel.addModel", "Less than max models");
				int intBestMatchIndex = -1;
				double dblBestMatchValue = -1;
				
				//compare all current models to new one
				for(int j = 0; j < colorModels.size(); j++){
					double redCorrelation = Imgproc.compareHist(colorModels.get(j).get(RED), newHistograms.get(RED), intCompareMethod);
					double greenCorrelation = Imgproc.compareHist(colorModels.get(j).get(GREEN), newHistograms.get(GREEN), intCompareMethod);
					double dblHistogramSimilarity = redCorrelation * greenCorrelation;
					
					if(dblHistogramSimilarity >= SIMILARITY_THRESHOLD && dblHistogramSimilarity > dblBestMatchValue){
						intBestMatchIndex = j;
						dblBestMatchValue = dblHistogramSimilarity;
					}
				}
				
				//if a similar one is found, update the most similar
				if(intBestMatchIndex > -1){
					colorModels.set(intBestMatchIndex, newHistograms);  //Updates the most similar histogram
					Log.i("RoadModel.addModel", "Updating existing histogram");
				}
				
				//Log.i("RoadModel.addModel", "# of Models: " + colorModels.size());
				//if no similar ones are found add the new one if there is room otherwise make room first
				if(colorModels.size() < MaxModels){
					//Log.i("RoadModel.addModel", "********************");
					//colorModels.add(newHistograms);
					colorModels.add(colorModels.size(), newHistograms);
				}
				else{
					colorModels.remove(0);  //remove oldest 
					//Log.i("RoadModel.addModel", "Removed old model");
					//colorModels.add(newHistograms);
					colorModels.add(colorModels.size(), newHistograms);
					
				}
			}
	}
}
