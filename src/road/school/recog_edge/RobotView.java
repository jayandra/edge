package road.school.recog_edge;



import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

import road.school.recog_edge.RobotControllerAdvanced.SteeringType;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;


class RobotView extends ViewBase {
	
	// m_Rgba matrix for holding camera image
	private Mat m_Rgba = new Mat();
	private Mat matRoadRegion = new Mat();

	
	public boolean paused = true;
	
	
	public SteeringType steer = SteeringType.SERVO;
	
	private RobotControllerAdvanced robot;

	
	public MessageReceiver messager;
	
	public Bitmap image = Bitmap.createBitmap(sf_CAMERA_WIDTH, sf_CAMERA_HEIGHT, Bitmap.Config.ARGB_8888);
	public Bitmap image2 = Bitmap.createBitmap(sf_CAMERA_WIDTH,sf_CAMERA_HEIGHT, Bitmap.Config.ARGB_8888);
	public int startx = 220, endx = 440, starty = 400, endy = 450;
	
	public RoadHistogram histo = new RoadHistogram();
	
	public static final int HISTOGRAM_SIZE = 32;
	public static final double ROAD_PROBABILITY_THRESHOLD = .5;
	private RoadModel roadModel;
	
	
	char[] msg;
	
	
	boolean ready = false;
	int[] inputs = new int[3];
	
	public Random rng = new Random();
	public int count = 0;
	
	private int intAggregateWidth = 32;
	private int intAggregateHeight = 32;
	
	public EdgeDetection edge_detection = new EdgeDetection();
	public LocalBinaryPattern local_binary_pattern = new LocalBinaryPattern();
	
	public boolean menu_edge_selected = true;
	public boolean menu_lbp_selected = false;
	public boolean menu_lbp_binary_selected = false;
	public boolean menu_lbp_binary_bin_selected = false;
	public boolean menu_lbp_gray_selected = false;
	public boolean menu_lbp_gray_bin_selected = false;
	
	/**
	 * Constructor for robot initialization
	 * 
	 * @param context
	 *            main context necessary for base class initialization
	 * @param ballColor
	 *            color that robot should be looking for
	 * @param MACAddress
	 *            bluetooth MAC address for arduino module
	 * @param serverAddress
	 *            server web address for sending and recieving data
	 * @param robotStyleSteering
	 *            tracked versus servo style steering
	 * @param userId
	 *            User Id of the individual robot for robot state initialization
	 *            and server communication
	 */
	public RobotView(Activity context, String MACAddress) {
		super(context);
		
		robot = new RobotControllerAdvanced(MACAddress);
		roadModel = new RoadModel();
		
		while(!robot.isConnected()){
			;
		}
		Log.e("RV", "Robot Connected");
	
		
	}

	

	
	
	/**
	 * When activity is placed to the background, disconnect the bluetooth
	 * connection and stop image processing
	 */
	public void pause() {
		this.m_IsActivityPaused = true;
		
	}


	/**
	 * Override function from base class that gets called for every image frame
	 */
	@Override
	protected void processFrame(VideoCapture capture) {
		
		capture.retrieve(m_Rgba, Highgui.CV_CAP_ANDROID_COLOR_FRAME_RGBA);
// Read from a image instead of camera
//Bitmap bmp =    BitmapFactory.decodeResource(getContext().getResources(), R.drawable.img1);
//bmp = Bitmap.createScaledBitmap(bmp, 640, 480, true);
//Utils.bitmapToMat(bmp, m_Rgba);
// End Read from a image instead of camera	
//
//getRoadRegion();
//updateColorModel(matRoadRegion);
//calculatePixelRoadProbabilities();
//
drawbox();
//
	//Mat temp = new Mat(new Size(100,100),Highgui.CV_CAP_ANDROID_COLOR_FRAME_RGBA);;
	//Mat temp_border = new Mat(new Size(105,105),Highgui.CV_CAP_ANDROID_COLOR_FRAME_RGBA);
	//Log.d("RESIZE_BEFORE", Integer.toString(m_Rgba.rows())+Integer.toString(m_Rgba.cols()));
	//Imgproc.resize(m_Rgba, temp, new Size(100,100));
	//Log.d("RESIZE_AFTER", Integer.toString(temp.rows())+Integer.toString(temp.cols()));
	//Imgproc.copyMakeBorder(temp, temp_border, 1,1,1,1, Imgproc.BORDER_CONSTANT);
	//Utils.matToBitmap(temp_border, image); // Converts OpenCV mat to Android bitmap

//Edge Detection begin
if(menu_edge_selected){
	List<MatOfPoint> road_contours;
	int road_contour;
	
	road_contours = edge_detection.compute_contours(m_Rgba);
	for(int i=0; i< road_contours.size(); i++){
		Imgproc.drawContours(m_Rgba, road_contours, i, new Scalar(255, 255, 255),2 );
	}
	edge_detection.find_bounderies(m_Rgba, road_contours);
//	road_contour = edge_detection.find_road(road_contours);
//	edge_detection.draw_contour(m_Rgba, road_contours, road_contour);
//	edge_detection.draw_overlay(m_Rgba, road_contours, road_contour);
	Utils.matToBitmap(m_Rgba, image);
}

if(menu_lbp_selected){
	Mat bw = m_Rgba.clone();
//Log.e("all booleans", 	Boolean.toString(menu_edge_selected)+"  "+
//						Boolean.toString(menu_lbp_selected)+"  "+	
//						Boolean.toString(menu_lbp_binary_selected)+"  "+
//						Boolean.toString(menu_lbp_binary_bin_selected)+"  "+
//						Boolean.toString(menu_lbp_gray_selected)+"  "+
//						Boolean.toString(menu_lbp_gray_bin_selected)+"  "
//	);	
	if(menu_lbp_binary_selected){
		bw = local_binary_pattern.computeLBP(m_Rgba, false, false);
		Mat histo = new Mat();
		histo = local_binary_pattern.compute_histogram(bw);
//		Log.e("----","binary");
	}
	if(menu_lbp_binary_bin_selected){
		bw = local_binary_pattern.computeLBP(m_Rgba, true, false);
//		Log.e("----","binary bin");
	}
	if(menu_lbp_gray_selected){
		bw = local_binary_pattern.computeLBP(m_Rgba, false, true);
//		Log.e("----","gray ");
	}
	if(menu_lbp_gray_bin_selected){
		bw = local_binary_pattern.computeLBP(m_Rgba, true, true);
//		Log.e("----","gray bin");
	}
	
	Utils.matToBitmap(bw, image);
}

// Edge detection end

// LBP end

				//Log.d("RESIZE_BEFORE", Integer.toString(bw.rows())+Integer.toString(bw.cols()));
				//Imgproc.resize(bw, bw, bw.size() );

//Log.d("RESIZE_AFTER", Integer.toString(bw.rows())+Integer.toString(bw.cols()));
//Utils.matToBitmap(bw, image);
//		Utils.matToBitmap(m_Rgba, image2);
		mBmpCanvas = image.copy(mBmpCanvas.getConfig(), true); //draws image to screen
       
		/*
		if (count < 20){
			count++;
		}
		else if( count < 25 ){
			handleImage();
			count++;
			Log.e("RV", "Count: " + count);
		}
		else if(count == 25){
			histo.findMax();
			count++;
			ready = true;
		}else{
			drawRoad();
		}
		
		drawbox();
		mBmpCanvas = image.copy(mBmpCanvas.getConfig(), true);
		if ( ready ){
			setInputs();
			takeAction();
		}
		*/
//resetInputs();
	}
	
	private void getRoadRegion(){
		matRoadRegion = m_Rgba.submat(startx, endx, starty, endy);
	}
	private void updateColorModel(Mat roadSection){
		List<Mat> images = new ArrayList<Mat>();
		images.add(roadSection);  // add image to list
		
		MatOfInt mChannels[] = new MatOfInt[] { new MatOfInt(0), new MatOfInt(1), new MatOfInt(2) };
        
        MatOfInt histSize = new MatOfInt(HISTOGRAM_SIZE);
        MatOfFloat ranges = new MatOfFloat(0f, 256f);
        
        List<Mat> matRGBHistograms = new ArrayList<Mat>();
        matRGBHistograms.add(new Mat());  //red histogram
        matRGBHistograms.add(new Mat());  //green histogram
        matRGBHistograms.add(new Mat());  //blue histogram
        
        //Imgproc.calcHist(images, channels, new Mat(), hist, histSize, ranges);
        for(int i=0; i<3; i++) {
        	Imgproc.calcHist(images, mChannels[i], new Mat(), matRGBHistograms.get(i), histSize, ranges);
        	Core.normalize(matRGBHistograms.get(i), matRGBHistograms.get(i));

        }
        
        roadModel.addModel(matRGBHistograms.get(0), matRGBHistograms.get(1));  //pass in with RGB order instead of openCV's BGR order
        
        
        Log.i("goo", "Start histogram");
        printHistogram(matRGBHistograms.get(0));
    	Log.i("goo", matRGBHistograms.get(0).dump());  //log blue histogram
    	Log.i("goo", matRGBHistograms.get(1).dump());  //log green histogram
    	Log.i("goo", matRGBHistograms.get(2).dump());  //log red histogram
		Log.i("goo", "End histogram");
		
		
	}
	
	private void printHistogram(Mat hist){
		for(int i = 0; i < 32; i ++){
			
			Log.i("goo", Double.toString(hist.get(i, 0)[0]));
		}
	}
	
	private void calculatePixelRoadProbabilities(){
		double green[] = {0.0,255.0,0.0,1.0};
		double red[] = {255.0,0.0,0.0, 1.0};
		double rgb[] = {}; 
		
		for(int x = 0; x < m_Rgba.cols(); x+=2){
			
			for(int y = 0; y < m_Rgba.rows()/3; y+=2){
				
				//m_Rgba.get(y, x, rgb);
				rgb = m_Rgba.get(y, x);
				Double dblRoadProbability = roadModel.calculateRoadProbability((int)rgb[0], (int)rgb[1]);
				
				//Log.i("RobotView.calculatePixelRoadProbabilities", "Red: " + Double.toString(rgb[0]) + "  Green: " + Double.toString(rgb[1]));
				//Log.i("RobotView.calculatePixelRoadProbabilities", Double.toString(dblRoadProbability));
				if(dblRoadProbability > ROAD_PROBABILITY_THRESHOLD ){
					m_Rgba.put(y, x, green);
					
					//image.setPixel(x, y, Color.GREEN);
				}
				else{
					
					m_Rgba.put(y, x, red);
					//image.setPixel(x, y, Color.RED);
				}
				
			}
		}/*
		m_Rgba.put(50, 50, red);
		m_Rgba.put(50, 51, red);
		m_Rgba.put(51, 50, red);
		m_Rgba.put(51, 51, red);
		*/
	}
	
	public void resetInputs(){
		
		inputs[0] = 0;
		inputs[1] = 0;
		inputs[2] = 0;
		
	}
	
	public void setInputs(){
		int inc = 640/3;
		int l = 0, s = 0, r = 0;
		for( int x = 0; x < inc; x++){
			for ( int y = 0; y < 480; y++){
				if(image.getPixel(x, y) == Color.WHITE){
					l++;
				}
			}
		}
		for( int x = 0; x < inc*2; x++){
			for ( int y = 0; y < 480; y++){
				if(image.getPixel(x, y) == Color.WHITE){
					s++;
				}
			}
		}
		for( int x = 0; x < inc*3; x++){
			for ( int y = 0; y < 480; y++){
				if(image.getPixel(x, y) == Color.WHITE){
					r++;
				}
			}
		}
		if( l > 1650 ){
			inputs[0] = 1;
		}
		if( s > 1650 ){
			inputs[1] = 1;
		}
		if( r > 1650 ){
			inputs[2] = 1;
		}
	}
	
	public void takeAction(){
		if( inputs[1] == 1){
			Log.e("Action", "Straight");
			//robot.driveVehicle(.5, .65, SteeringType.SERVO);
			robot.driveForwardSlow();
		}
		else if( inputs[0] == 1){
			Log.e("Action", "Left");
			//robot.driveVehicle(.25, .65, SteeringType.SERVO);
			robot.trnLeftSlow();
		}
		else if( inputs[2] == 1){
			Log.e("Action", "Right");
			//robot.driveVehicle(.75, .65, SteeringType.SERVO);
			robot.turnRightSlow();
		}
		else{
			Log.e("Action", "Stop");
			//robot.driveVehicle(.5, .5, SteeringType.SERVO);
			robot.stop();
		}
	}
	
	
	public void handleMessage(){
		msg = new char[4];
		msg = messager.read();
		Log.e("MR-HM", "message: " + msg[0] + " "+ msg[1] + " "+ msg[2] + " "+ msg[3]);
		if (msg[0] == 'D'){
			drive(msg[1], msg[2]);
		}
	}
	
	public void handleImage(){
		int cr, cg;
		for ( int x = startx; x < endx; x+=4){
			for( int y = starty; y < endy; y+=4 ){
				int px = x + 2;
				int py = y + 2;
				if( px > endx ){
					px = endx;
				}
				if( py > endy ){
					py = endy;
				}
				cr = 0;
				cg = 0;
				cr = Color.red(image2.getPixel(x, y));
				cg = Color.green(image2.getPixel(x, y));

				histo.addPixel(cr, cg);
				
				if( x == startx || y == starty || x == endx-1|| y == endy-1){
					image.setPixel(x, y, Color.RED);
				}
				
			}
		}
	}
	
	
	public void drawRoad(){
		int cr, cg;
		for ( int x = 0 ; x < image.getWidth(); x+=8){
			for( int y = 0; y < image.getHeight(); y+=8 ){
				int px = x + 4;
				int py = y + 4;
				
				if( px > image.getWidth() ){
					px = image.getWidth();
				}
				if( py > image.getHeight() ){
					py = image.getHeight();
				}
				
				
				cr = Color.red(image.getPixel(px, py));
				cg = Color.green(image.getPixel(px, py));
				if( histo.getRoadDegree(cr, cg) > .005){
					for (int i = x; i < x+8; i++){
						for (int j = y; j < y+8; j++){
							if( i > image.getWidth() ){
								i = image.getWidth();
							}
							if( j > image.getHeight() ){
								j = image.getHeight();
							}
							image.setPixel(i, j, Color.WHITE);
						}
					}
		
					
				}
				
			}
		}
		
	}
	
	
	public void drawbox(){
		//double blue[] = {0.0,0.0,255.0,1.0};
		
		for ( int x = startx; x < endx; x++){
			for( int y = starty; y < endy; y++ ){
				if( x == startx || y == starty || x == endx-1|| y == endy-1){
					image.setPixel(x, y, Color.BLUE);
					//m_Rgba.put(y, x, blue);
				}
				
			}
		}
	}
	public void drive( char d, char p ){
		
		double dir = d/180.0;
		double pow = p/180.0;
		Log.e("RV-Drive", "direction: " + dir + " power " + pow);
		robot.driveVehicle(dir, pow, steer);
	}

	
	
	
}
