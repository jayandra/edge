package road.school.recog_edge;



import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;
import cotsbots.devicedescovery.BluetoothDiscoveryActivity;

public class RoadRecognitionActivity extends Activity {
    

	RobotView robot;
	Intent macIntent;
	public String macAdd;
	
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(this.getClass().getSimpleName(), "OpenCV loaded successfully");
                    startRobot();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //macIntent = new Intent(this, BluetoothDiscoveryActivity.class);
        //startActivityForResult(macIntent, BluetoothDiscoveryActivity.sf_REQUEST_CODE_BLUETOOTH);
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this, mLoaderCallback);
        //startRobot();
    }
    
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == BluetoothDiscoveryActivity.sf_REQUEST_CODE_BLUETOOTH) {
			setContentView(R.layout.main);
			if (resultCode == RESULT_OK) {
				macAdd = data.getStringExtra(BluetoothDiscoveryActivity.sf_SELECTED_MAC_ADDRESS);
				startRobot();
			}
			else{
				Toast.makeText(RoadRecognitionActivity.this, "Did not connect to Bot", Toast.LENGTH_LONG).show();
			}
		}
		
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Edge");
        SubMenu lbp = menu.addSubMenu("LBP");
        lbp.add("Binary");
        lbp.add("Binary_bin");
        lbp.add("Gray");
        lbp.add("Gray_bin");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	robot.menu_edge_selected = false;
    	robot.menu_lbp_selected = false;
    	robot.menu_lbp_binary_selected = false;
    	robot.menu_lbp_binary_bin_selected = false;
    	robot.menu_lbp_gray_selected = false;
    	robot.menu_lbp_gray_bin_selected = false;
    	
    	if(item.toString() == "Edge"){
    		call_edge_detection();
            return true;
    	}
    	else if(item.toString() == "Binary" || item.toString() == "Binary_bin" || item.toString() == "Gray" || item.toString() == "Gray_bin"){
    		call_lbp(item.toString());
            return true;
    	}
    	else{
    		return super.onOptionsItemSelected(item);
    	}
    }
    
    public void startRobot(){
    	robot = new RobotView(this, macAdd);

    	ViewGroup layout = (ViewGroup) findViewById(R.id.camera);
    	layout.addView(robot);
    	
    	call_edge_detection();
    }
    
    public void call_edge_detection(){
    	SeekBar yourSeekBar=(SeekBar) findViewById(R.id.edge_threshold);
    	yourSeekBar.setVisibility(View.VISIBLE);
    	robot.menu_edge_selected = true;
    	yourSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
			@Override
			public void onProgressChanged(SeekBar yourSeekBar, int progress, boolean user_generated) {
//				Log.e("Progress bar", Integer.toString(progress));
				robot.edge_detection.LowerThreshold = progress;
				// TODO Auto-generated method stub		
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
			}
    	});
    }
    
    public void call_lbp(String sub_menu){
    	SeekBar yourSeekBar=(SeekBar) findViewById(R.id.edge_threshold);
    	yourSeekBar.setVisibility(View.GONE);
    	robot.menu_lbp_selected = true;

    	if(sub_menu == "Binary")
    		robot.menu_lbp_binary_selected = true;
    	if(sub_menu == "Binary_bin")
    		robot.menu_lbp_binary_bin_selected = true;
    	if(sub_menu == "Gray")
    		robot.menu_lbp_gray_selected = true;
    	if(sub_menu == "Gray_bin")
    		robot.menu_lbp_gray_bin_selected = true; 	
    }
    
}