/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package road.school.recog_edge;

public final class R {
    public static final class attr {
        /** <p>May be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
<p>May be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>any</code></td><td>-1</td><td></td></tr>
<tr><td><code>back</code></td><td>99</td><td></td></tr>
<tr><td><code>front</code></td><td>98</td><td></td></tr>
</table>
         */
        public static final int camera_id=0x7f010001;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int show_fps=0x7f010000;
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
    }
    public static final class id {
        public static final int any=0x7f040000;
        public static final int back=0x7f040001;
        public static final int buttonNewMAC=0x7f040004;
        public static final int button_cancel=0x7f040007;
        public static final int button_scan=0x7f040006;
        public static final int camera=0x7f04000f;
        public static final int control=0x7f040010;
        public static final int edge_threshold=0x7f040011;
        public static final int editText1=0x7f040005;
        public static final int front=0x7f040002;
        public static final int new_devices=0x7f04000d;
        public static final int paired_devices=0x7f04000c;
        public static final int robotLayout=0x7f04000e;
        public static final int tableRow1=0x7f040008;
        public static final int tableRow2=0x7f04000b;
        public static final int tableRow3=0x7f040003;
        public static final int title_new_devices=0x7f04000a;
        public static final int title_paired_devices=0x7f040009;
    }
    public static final class layout {
        public static final int device_list=0x7f030000;
        public static final int device_name=0x7f030001;
        public static final int main=0x7f030002;
    }
    public static final class string {
        public static final int app_name=0x7f05000c;
        public static final int button_cancel=0x7f050007;
        public static final int button_scan=0x7f050006;
        public static final int hello=0x7f05000b;
        public static final int macAddressHint=0x7f050009;
        public static final int macAddressHintError=0x7f05000a;
        public static final int macAddressNew=0x7f050008;
        public static final int none_found=0x7f050003;
        public static final int none_paired=0x7f050002;
        /**   DeviceListActivity 
         */
        public static final int scanning=0x7f050000;
        public static final int select_device=0x7f050001;
        public static final int title_other_devices=0x7f050005;
        public static final int title_paired_devices=0x7f050004;
    }
    public static final class styleable {
        /** Attributes that can be used with a CameraBridgeViewBase.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #CameraBridgeViewBase_camera_id road.school.recog_edge:camera_id}</code></td><td></td></tr>
           <tr><td><code>{@link #CameraBridgeViewBase_show_fps road.school.recog_edge:show_fps}</code></td><td></td></tr>
           </table>
           @see #CameraBridgeViewBase_camera_id
           @see #CameraBridgeViewBase_show_fps
         */
        public static final int[] CameraBridgeViewBase = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link road.school.recog_edge.R.attr#camera_id}
          attribute's value can be found in the {@link #CameraBridgeViewBase} array.


          <p>May be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
<p>May be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>any</code></td><td>-1</td><td></td></tr>
<tr><td><code>back</code></td><td>99</td><td></td></tr>
<tr><td><code>front</code></td><td>98</td><td></td></tr>
</table>
          @attr name road.school.recog_edge:camera_id
        */
        public static final int CameraBridgeViewBase_camera_id = 1;
        /**
          <p>This symbol is the offset where the {@link road.school.recog_edge.R.attr#show_fps}
          attribute's value can be found in the {@link #CameraBridgeViewBase} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name road.school.recog_edge:show_fps
        */
        public static final int CameraBridgeViewBase_show_fps = 0;
    };
}
